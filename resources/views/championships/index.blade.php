@extends('layouts/frontend')
@section('content')

<section id="content">
		<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><a href="#" class="silver">Men’s </a><span class="color">Tennis</span>
			</div>
			<div class="content-pages">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<div class="sidebar-left sidebar-shop">
							<div class="widget widget-category-icon">
								<h2 class="title-widget title18">Categories</h2>
								<div class="list-cat-icon2">
									<ul class="list-none">
										<li><a href="#"><img src="images/icons/cat1.png" alt="">Bikes Ovicsport store <span class="silver">(9)</span></a></li>
										<li><a href="#"><img src="images/icons/cat2.png" alt="">Basketball Sport store<span class="silver">(32)</span></a></li>
										<li><a href="#"><img src="images/icons/cat3.png" alt="">Weightlifting Ovicsport <span class="silver">(19)</span></a></li>
										<li><a href="#"><img src="images/icons/cat4.png" alt="">Volleyball Store <span class="silver">(26)</span></a></li>
										<li><a href="#"><img src="images/icons/cat5.png" alt="">Football Ovicsport <span class="silver">(5)</span></a></li>
										<li><a href="#"><img src="images/icons/cat6.png" alt="">Golf Ovicsport store <span class="silver">(9)</span></a></li>
										<li><a href="#"><img src="images/icons/cat8.png" alt="">Rowing store <span class="silver">(4)</span></a></li>
										<li><a href="#"><img src="images/icons/cat9.png" alt="">Runner moving<span class="silver">(7)</span></a></li>
									</ul>
								</div>
							</div>
							<!-- End Widget -->
							<div class="widget widget-filter">
								<h2 class="title-widget title18">Shop By</h2>
								<div class="current-shop">
									<h3 class="title14">CURRENTLY SHOPPING BY:</h3>
									<ul class="list-none">
										<li><a href="#" class="silver"> Golf Ovicsport store</a></li>
									</ul>
									<a href="#" class="clear-all black">Clear All</a>
								</div>
								<h3 class="title14 shop-option">SHOPPING OPTIONS</h3>
								<div class="filter-price">
									<h3 class="title14">Price</h3>
									<div class="range-filter">
										<span class="currency-index">$</span>
										<span class="amount"></span>
										<button class="btn-filter-price shop-button title14">Filter</button>
										<div class="slider-range"></div>
									</div>
								</div>
								<!-- End Filter Price -->
								<div class="filter-manufacture">
									<h3 class="title14">Manufacturer</h3>
									<ul class="list-none">
										<li><a href="#">Addidas Design <span class="silver">(9)</span></a></li>
										<li><a href="#">Puma Design<span class="silver">(25)</span></a></li>
										<li><a href="#">Nike design<span class="silver">(07)</span></a></li>
										<li><a href="#">Other<span class="silver">(32)</span></a></li>
									</ul>
								</div>
								<!-- End Filter Manufacture -->
								<div class="filter-color">
									<h3 class="title14">Color</h3>
									<ul class="list-inline-block">
										<li><a href="#" style="background:#ffffff;border:1px solid #e5e5e5"></a></li>
										<li><a href="#" style="background:#e66054"></a></li>
										<li><a class="active" href="#" style="background:#d0b7cc"></a></li>
										<li><a href="#" style="background:#107a8e"></a></li>
										<li><a href="#" style="background:#b9cad2"></a></li>
										<li><a href="#" style="background:#a7bc93"></a></li>
										<li><a href="#" style="background:#d3b627"></a></li>
										<li><a href="#" style="background:#b4b3ae"></a></li>
										<li><a href="#" style="background:#502006"></a></li>
										<li><a href="#" style="background:#311e21"></a></li>
										<li><a href="#" style="background:#e6b3af"></a></li>
										<li><a href="#" style="background:#f3d213"></a></li>
										<li><a href="#" style="background:#bd0316"></a></li>
										<li><a href="#" style="background:#ff607c"></a></li>
									</ul>
								</div>
							</div>
							<!-- End Widget -->
							<div class="widget widget-compare">
								<h2 class="title-widget title18">Compare Products</h2>
								<ul class="list-none">
									<li>
										<div class="item-product-compare table">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link">
													<img src="images/photos/sport_13.jpg" alt="">
												</a>
											</div>
											<div class="product-info">
												<h3><a href="#">Bikes Ovicsport store <span class="silver">(1)</span></a></h3>
											</div>
										</div>
									</li>
								</ul>
								<ul class="list-inline-block clear-compare">
									<li><a href="#" class="shop-button title14">Compare</a></li>
									<li><a href="#" class="title14">Clear All</a></li>
								</ul>
							</div>	
						</div>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-shop shop-grid">
							<div class="shop-title-box">
								<h2 class="title18 title-box5">Men’s</h2>
								<div class="view-type">
									<a href="grid.html" class="grid-view"></a>
									<a href="list.html" class="list-view active"></a>
								</div>
							</div>
							<div class="shop-banner banner-adv line-scale">
								<a href="#" class="adv-thumb-link"><img src="images/shop/banner.jpg" alt="" /></a>
							</div>
							<div class="list-shop-product">
								@foreach($championships as $championship)
									<div class="item-product item-product-list">
										<div class="row">
											<div class="col-md-4 col-sm-5 col-xs-5">
												<div class="product-thumb">
													<span class="product-label sale-label">50% off</span>
													<a href="detail.html" class="product-thumb-link">
														<img src="images/photos/sport_13.jpg" alt="">
													</a>
												</div>
											</div>
											<div class="col-md-8 col-sm-7 col-xs-7">
												<div class="product-info">
													<h3 class="product-title title14"><a href="detail.html">{{$championship->title}}</a></h3>
													<div class="product-price">
														<del><span class="title14 silver">$798.00</span></del>
														<ins><span class="title14 color">$399.00</span></ins>
													</div>
													<div class="product-rate">
														<div class="product-rating" style="width:100%"></div>
													</div>
													<span class="inout-stock in-stock"><i class="fa fa-check-square" aria-hidden="true"></i>In stock</span>
													<p class="desc">{{$championship->content}} </p>
													<div class="product-extra-link">
														<a href="#" class="addcart-link">Add to cart</a>
														<a href="#" class="wishlist-link"></a>
														<a href="#" class="compare-link"></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
							<div class="sort-paginav pull-right">
								<div class="sort-bar select-box">
									<select>
										<option value="">position</option>
										<option value="">price</option>
									</select>
								</div>
								<div class="show-bar select-box">
									<label>Show:</label>
									<select>
										<option value="">6</option>
										<option value="">12</option>
										<option value="">24</option>
									</select>
								</div>
								<div class="pagi-bar">
									<a href="#" class="current-page">1</a>
									<a href="#">2</a>
									<a href="#">3</a>
									<a href="#" class="next-page">next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="list-service">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
									<h4 class="title14 transition">Hours: 8AM -11PM</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block item-active active">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
									<h4 class="title14 transition">When you use credit card</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
									<h4 class="title14 transition">On orders over $99</h4>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End List Service -->
			<div class="list-special-box">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Specials</h2>
							<div class="product-special">
								<div class="special-slider">
									<div class="wrap-item navi-bottom" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1]]">
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_1.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_2.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_3.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
									</div>
								</div>
								<a href="#" class="shop-button">View all specials</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Newsletter</h2>
							<div class="newletter-form">
								<p class="desc">Make sure you dont miss interesting hap penings by joining our newsletter program.</p>
								<form class="email-form">
									<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="your e-mail address" type="text">
									<input class="shop-button" value="Subscribe" type="submit">
								</form>
							</div>
							<h2 class="title18 font-bold">Connect with us</h2>
							<div class="social-network">
								<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt="" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 hidden-sm col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Video sport</h2>
							<div class="box-video">
								<a href="#" class="video-lightbox"><img src="images/home/home1/video-img.png" alt="" /></a>
								<h3 class="title14"><a href="#">Lorem ipsum dolor sit amet</a></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
	</section>
	<!-- End Content -->

@endsection