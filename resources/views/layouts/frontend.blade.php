<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Sport is new Html theme that we have designed to help you transform your store into a beautiful online showroom. This is a fully responsive Html theme, with multiple versions for homepage and multiple templates for sub pages as well" />
	<meta name="keywords" content="Sport,7uptheme" />
	<meta name="robots" content="noodp,index,follow" />
	<meta name='revisit-after' content='1 days' />
	<title>Sport | Home Style 3</title>
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/libs/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/bootstrap-theme.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/jquery-ui.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.carousel.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.transitions.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.theme.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/flipclock.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/animate.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/hover.css"/>
	<link rel="stylesheet" type="text/css" href="css/color.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/theme.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/browser.css" media="all"/>
	<!-- <link rel="stylesheet" type="text/css" href="css/rtl.css" media="all"/> -->
</head>
<body>
<div class="wrap">
	<header id="header">
		<div class="header-main3">
			<div class="row">
				<div class="col-sm-3 col-xs-7">
					<div class="logo logo3">
						<h1 class="hidden">Sports</h1>
						<a href="index.html"><img src="images/home/home1/logo.png" alt="" /></a>
					</div>
				</div>
				<div class="col-md-9 col-sm-9 col-xs-5">
					<div class="account-cart3 pull-right">
						<ul class="list-inline-block top-menu inline-block">
							
							@guest
	                            <li class="nav-item">
	                                <a class="title14 black" href="{{ route('login') }}">{{ __('Login') }}</a>
	                            </li>
                       		@else
                       			<li><a href="#" class="title14 black">My acount</a></li>
                           		<li>
		                                <a class="title14 black" href="{{ route('logout') }}"
	                                       onclick="event.preventDefault();
	                                                     document.getElementById('logout-form').submit();">
	                                        {{ __('Logout') }}
	                                    </a>

	                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>
	                            </li>
                       		@endguest
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Header Main -->
		<div class="header-nav3">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<nav class="main-nav main-nav3">
						<ul class="list-none">
							<li><a href="{{asset('/')}}">home</a></li>
							<li><a href="{{asset('/aboutus')}}">About</a></li>
							<li><a href="{{asset('/campionate')}}">Campionate</a></li>
							<li><a href="{{asset('/contact')}}">Contact</a></li>
						</ul>
						<a href="#" class="toggle-mobile-menu"><span></span></a>
					</nav>
					<!-- End Main nav -->
					<div class="wrap-language-search pull-right">
						<div class="smart-search inline-block">
							<span class="title14 search-label">search</span>
							<div class="select-search-category">
								<button class="btn-search-cat"><span>All</span></button>
								<ul class="list-cat-search list-none">
									<li><a href="#">Bikes Ovicsport store</a></li>
									<li><a href="#">Weightlifting Ovicsport </a></li>
									<li><a href="#">Volleyball Store </a></li>
									<li><a href="#">Football Ovicsport </a></li>
									<li><a href="#">Golf Ovicsport store</a></li>
									<li><a href="#">Rowing store</a></li>
									<li><a href="#">Runner moving </a></li>
								</ul>
							</div>
							<form class="smart-search-form">
								<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Search for products" type="text">
								<input value="" type="submit">
							</form>
						</div>
						<ul class="list-inline-block top-currency-language inline-block">
							<li>
								<div class="language-box">
									<a href="#" class="language-current title14 black"><span>English</span><i class="fa fa-angle-down" aria-hidden="true"></i> </a>
									<ul class="language-list list-none">	
										<li><a href="#"><img src="images/icons/flag-en.png" alt=""><span>English</span></a></li>
										<li><a href="#"><img src="images/icons/flag-fr.png" alt=""><span>French</span></a></li>
										<li><a href="#"><img src="images/icons/flag-sp.png" alt=""><span>Spanish</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	@yield("content")
	<footer id="footer">
		<div class="footer">
			<div class="main-footer3">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-8 col-xs-12">
							<div class="footer-box">
								<div class="logo logo-footer3">
									<a href="index.html"><img src="images/home/home1/logo.png" alt="" /></a>
									<p class="silver">Mauris sed mauris bibendum est imperdiet </p>
								</div>
								<div class="social-network">
									<h2 class="title18 font-bold">Connect with us</h2>
									<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt=""></a>
									<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt=""></a>
									<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt=""></a>
									<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt=""></a>
									<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt=""></a>
									<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt=""></a>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<div class="footer-box">
								<h2 class="title14 black">My account</h2>
								<ul class="list-none menu-foter">
									<li><a href="#">My Orders</a></li>
									<li><a href="#">My Credit Slips</a></li>
									<li><a href="#">My Addresses</a></li>
									<li><a href="#">My Personal Info</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<div class="footer-box">
								<h2 class="title14 black">Orders</h2>
								<ul class="list-none menu-foter">
									<li><a href="#">Payment options</a></li>
									<li><a href="#">Shipping and delivery</a></li>
									<li><a href="#">Returns</a></li>
									<li><a href="#">Shipping</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<div class="footer-box">
								<h2 class="title14 black">Information</h2>
								<ul class="list-none menu-foter">
									<li><a href="#">Specials</a></li>
									<li><a href="#">New Products</a></li>
									<li><a href="#">Best Sellers</a></li>
									<li><a href="#">Our Stores</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2 col-sm-4 col-xs-12">
							<div class="footer-box">
								<h2 class="title14 black">Contact us</h2>
								<p class="desc contact-info-footer"><span class="silver"><i class="fa fa-map-marker" aria-hidden="true"></i></span>Sesam Street 323b, 4010, Norway</p>
								<p class="desc contact-info-footer"><span class="silver"><i class="fa fa-phone" aria-hidden="true"></i></span>003 118 4563 560</p>
								<p class="desc contact-info-footer"><span class="silver"><i class="fa fa-envelope-o" aria-hidden="true"></i></span><a href="maillto:support247@storename.com">support247@storename.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Main Footer -->
			<div class="footer-bottom3">
				<div class="container">
					<div class="footer-tags">
						<label class="title14">Shoes</label>
						<a href="#" class="silver">Men's Tops & T-Shirts </a>
						<a href="#" class="silver">Men's Sweatshirts</a>
						<a href="#" class="silver">Men's Jackets & Outerwear</a>
						<a href="#" class="silver">Men's Golf Apparel</a>
						<a href="#" class="silver">Men's Shortsl</a>
						<a href="#" class="silver">Running</a>
						<a href="#" class="silver">Shop By Brand</a>
						<a href="#" class="silver">Boots</a>
						<a href="#" class="silver">Training</a>
						<a href="#" class="silver">Cleats</a>
					</div>
					<div class="footer-tags">
						<label class="title14">Fan shop</label>
						<a href="#" class="silver">Arizona Cardinals</a>
						<a href="#" class="silver">Atlanta Falcons</a>
						<a href="#" class="silver">Baltimore Ravens</a>
						<a href="#" class="silver">Buffalo Bills</a>
						<a href="#" class="silver">Carolina Panthers</a>
						<a href="#" class="silver">Atlanta Hawks</a>
						<a href="#" class="silver">Brooklyn Nets</a>
						<a href="#" class="silver">Boston Celtics </a>
						<a href="#" class="silver">Charlotte Bobcats</a>
						<a href="#" class="silver">Charlotte Hornets</a>
					</div>
					<div class="footer-tags">
						<label class="title14">Accessories</label>
						<a href="#" class="silver">Backpacks</a>
						<a href="#" class="silver">Sack Packs </a>
						<a href="#" class="silver">Duffel and Gym Bags</a>
						<a href="#" class="silver">Tote Bags</a>
						<a href="#" class="silver">Backpacking Packs </a>
						<a href="#" class="silver">Fitness Trackers </a>
						<a href="#" class="silver">Pedometers</a>
						<a href="#" class="silver">Heart Rate Monitors</a>
						<a href="#" class="silver">GPS Watches </a>
						<a href="#" class="silver">Compare Activity Monitors</a>
					</div>
				</div>
			</div>
			<!-- End Footer Bottom -->
		</div>
	</footer>
	<!-- End Footer -->
	<div class="wishlist-mask">
		<div class="wishlist-popup">
			<span class="popup-icon"><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
			<p class="wishlist-alert">"Sport Product" was added to wishlist</p>
			<div class="wishlist-button">
				<a href="#">Continue Shopping (<span class="wishlist-countdown">10</span>)</a>
				<a href="#">Go To Shopping Cart</a>
			</div>
		</div>
	</div>
	<!-- End Wishlist Mask -->
	<a href="#" class="scroll-top rect"><i class="fa fa-angle-up" aria-hidden="true"></i><span>TOP</span></a>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/jquery.fancybox.js"></script>
<script src="js/libs/jquery-ui.min.js"></script>
<script src="js/libs/owl.carousel.min.js"></script>
<script src="js/libs/jquery.jcarousellite.min.js"></script>
<script src="js/libs/jquery.elevatezoom.js"></script>
<script src="js/libs/jquery.mCustomScrollbar.js"></script>
<script src="js/libs/jquery.bxslider.js"></script>
<script src="js/libs/slick.js"></script>
<script src="js/libs/popup.js"></script>
<script src="js/libs/flipclock.js"></script>
<script src="js/libs/wow.js"></script>
<script src="js/theme.js"></script>