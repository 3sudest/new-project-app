@extends("layouts/frontend")
@section("content")

<section id="content">
		<div class="container">
			<div class="bread-crumb">
				<a href="#" class="silver">Home</a><span class="color">About Us</span>
			</div>
			<div class="content-pages">
				<div class="about-intro text-center">
					<h2 class="title18 line-image">Ovancsport</h2>
					<h2 class="title30 text-center">Information</h2>
					<p class="desc">Lorem ipsum dolor sit amet conse ctetur adipisicing lit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venia.</p>
				</div>
				<!-- End Intro -->
				<div class="about-service">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="item-about-service text-center">
								<div class="about-service-icon">
									<a href="#"><i class="fa fa-desktop" aria-hidden="true"></i></a>
								</div>
								<h3 class="title18"><a href="#" class="black">Responsive Design</a></h3>
								<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="item-about-service text-center">
								<div class="about-service-icon">
									<a href="#"><i class="fa fa-tachometer" aria-hidden="true"></i></a>
								</div>
								<h3 class="title18"><a href="#" class="black">Responsive Design</a></h3>
								<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="item-about-service text-center">
								<div class="about-service-icon">
									<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i></a>
								</div>
								<h3 class="title18"><a href="#" class="black">Responsive Design</a></h3>
								<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End Service -->
				<div class="choise-faq">
					<div class="choise-intro text-center">
						<h2 class="title18">Why Choose <span class="silver">Ovancsport</span></h2>
						<p class="desc">Today more than hundreds of customers worldwide are using our products from personal<br> websites to midsized companies... They have reasons to trust us!</p>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="about-faq toggle-tab">
								<div class="item-toggle-tab">
									<h2 class="toggle-tab-title title18 silver">At vero eos et accusamus et iusto </h2>
									<div class="toggle-tab-content">
										<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio.</p>
									</div>
								</div>
								<div class="item-toggle-tab">
									<h2 class="toggle-tab-title title18 silver">Dignissimos ducimus qui blanditiis </h2>
									<div class="toggle-tab-content">
										<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio.</p>
									</div>
								</div>
								<div class="item-toggle-tab">
									<h2 class="toggle-tab-title title18 silver">Raesentium voluptatum deleniti </h2>
									<div class="toggle-tab-content">
										<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio.</p>
									</div>
								</div>
								<div class="item-toggle-tab">
									<h2 class="toggle-tab-title title18 silver">At vero eos et accusamus et iusto </h2>
									<div class="toggle-tab-content">
										<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio.</p>
									</div>
								</div>
								<div class="item-toggle-tab">
									<h2 class="toggle-tab-title title18 silver">Dignissimos ducimus qui blanditiis </h2>
									<div class="toggle-tab-content">
										<p class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi architecto aspernatur assumenda cum distinctio id incidunt inventore labore magnam odio.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="about-thumb pull-right">
								<a href="#"><img src="images/pages/img-about.png" alt="" /></a>
							</div>
						</div>
					</div>
				</div>
				<!-- End Choise -->
				<div class="about-intro text-center">
					<h2 class="title18 line-image">Ovancsport</h2>
					<h2 class="title30">Team members</h2>
					<p class="desc">Lorem ipsum dolor sit amet conse ctetur adipisicing lit, sed do eiusmod tempor incididunt. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venia.</p>
				</div>
				<!-- End Intro -->
				<div class="about-team-member">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-6">
							<div class="item-about-member text-center">
								<div class="about-member-thumb banner-adv zoom-image">
									<a href="#" class="adv-thumb-link"><img src="images/pages/img-about-8.png" alt="" /></a>
									<div class="member-social">
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-pinterest-p"></i></a>
										<a href="#"><i class="fa fa-google-plus"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
								<div class="about-member-info">
									<h3 class="title18"><a href="#">Great Illustration</a></h3>
									<span class="silver">Creative Director</span>
									<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus quisent ipsum. Thank you!</p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<div class="item-about-member text-center">
								<div class="about-member-thumb banner-adv zoom-image">
									<a href="#" class="adv-thumb-link"><img src="images/pages/img-about-5.png" alt="" /></a>
									<div class="member-social">
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-pinterest-p"></i></a>
										<a href="#"><i class="fa fa-google-plus"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
								<div class="about-member-info">
									<h3 class="title18"><a href="#">Great Illustration</a></h3>
									<span class="silver">Creative Director</span>
									<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus quisent ipsum. Thank you!</p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<div class="item-about-member text-center">
								<div class="about-member-thumb banner-adv zoom-image">
									<a href="#" class="adv-thumb-link"><img src="images/pages/img-about-6.png" alt="" /></a>
									<div class="member-social">
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-pinterest-p"></i></a>
										<a href="#"><i class="fa fa-google-plus"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
								<div class="about-member-info">
									<h3 class="title18"><a href="#">Great Illustration</a></h3>
									<span class="silver">Creative Director</span>
									<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus quisent ipsum. Thank you!</p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<div class="item-about-member text-center">
								<div class="about-member-thumb banner-adv zoom-image">
									<a href="#" class="adv-thumb-link"><img src="images/pages/img-about-7.png" alt="" /></a>
									<div class="member-social">
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-pinterest-p"></i></a>
										<a href="#"><i class="fa fa-google-plus"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
								<div class="about-member-info">
									<h3 class="title18"><a href="#">Great Illustration</a></h3>
									<span class="silver">Creative Director</span>
									<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus quisent ipsum. Thank you!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Team Member -->
			</div>
			<div class="list-service">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form1.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Order Online</a></h3>
									<h4 class="title14 transition">Hours: 8AM -11PM</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block item-active active">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form2.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Save 30% </a></h3>
									<h4 class="title14 transition">When you use credit card</h4>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<ul class="item-service list-inline-block">
							<li>
								<div class="service-icon">
									<a href="#"><img class="wobble-horizontal" src="images/home/home1/form3.png" alt="" /></a>
								</div>
							</li>
							<li>
								<div class="service-info">
									<h3 class="title18 font-bold"><a href="#" class="black">Free Shipping</a></h3>
									<h4 class="title14 transition">On orders over $99</h4>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End List Service -->
			<div class="list-special-box">
				<div class="row">
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Specials</h2>
							<div class="product-special">
								<div class="special-slider">
									<div class="wrap-item navi-bottom" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1]]">
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_1.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_2.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
										<div class="item-product">
											<div class="product-thumb">
												<a href="detail.html" class="product-thumb-link"><img src="images/photos/sport_3.jpg" alt="" /></a>
											</div>
											<div class="product-info">
												<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
												<div class="product-price">
													<del><span class="title14 silver">$798.00</span></del>
													<ins><span class="title14 color">$399.00</span></ins>
												</div>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<p class="desc">Duis lobortis dui lacus, eget rutrum erat congue at. </p>
											</div>
										</div>
									</div>
								</div>
								<a href="#" class="shop-button">View all specials</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Newsletter</h2>
							<div class="newletter-form">
								<p class="desc">Make sure you dont miss interesting hap penings by joining our newsletter program.</p>
								<form class="email-form">
									<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="your e-mail address" type="text">
									<input class="shop-button" value="Subscribe" type="submit">
								</form>
							</div>
							<h2 class="title18 font-bold">Connect with us</h2>
							<div class="social-network">
								<a href="#" class="float-shadow"><img src="images/icons/icon-fb.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-tw.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-li.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-gp.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-pt.png" alt="" /></a>
								<a href="#" class="float-shadow"><img src="images/icons/icon-sk.png" alt="" /></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 hidden-sm col-xs-12">
						<div class="special-box border">
							<h2 class="title18 font-bold">Video sport</h2>
							<div class="box-video">
								<a href="#" class="video-lightbox"><img src="images/home/home1/video-img.png" alt="" /></a>
								<h3 class="title14"><a href="#">Lorem ipsum dolor sit amet</a></h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Special Box -->
		</div>
	</section>
	<!-- End Content -->


@endsection
