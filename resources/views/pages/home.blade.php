@extends("layouts/frontend")
@section("content")

<!-- End Header -->
	<section id="content">
		<div class="banner-slider banner-slick banner-slider3">
			<div class="slick center">
				<div class="item-slider">
					<div class="banner-thumb">
						<a href="#"><img src="images/home/home3/slide1.jpg" alt="" /></a>
					</div>
					<div class="banner-info">
						<div class="banner-info3 white text-center white">
							<h2 class="title48 font-bold">GAME, SET, HOLIDAYS</h2>
							<h3 class="title18">Aenean commodo ligula eget dolor.  fermentum placerat vitae eu odioGumbo beet greens corn soko endive gumbo gourd consectetuer adipiscing.</h3>
							<a href="#" class="btn-arrow title14"><span>shop now </span></a>
						</div>
					</div>
					<p class="desc-control hidden">Suspendisse eleme ntum elemenm</p>
				</div>
				<div class="item-slider">
					<div class="banner-thumb">
						<a href="#"><img src="images/home/home3/slide2.jpg" alt="" /></a>
					</div>
					<div class="banner-info">
						<div class="banner-info3 white text-center white">
							<h2 class="title48 font-bold">MEN'S COLLECTION</h2>
							<h3 class="title18">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo fermentum placerat vitae eu odioGumbo beet greens corn soko endive gourd.</h3>
							<a href="#" class="btn-arrow title14"><span>shop now </span></a>
						</div>
					</div>
					<p class="desc-control hidden">Eleme ntum suspendisse  elemenm</p>
				</div>
			</div>
		</div>
		<!-- End Banner Slider -->
		<div class="container">	
			<div class="list-ads3">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="banner-adv item-adv3 zoom-image">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ad1.jpg" alt="" /></a>
							<div class="banner-info text-center">
								<h2 class="title30 white">shoes men</h2>
								<a href="#" class="title14 btn-caret white">shop now <i class="fa fa-caret-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="banner-adv item-adv3 zoom-image">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ad2.jpg" alt="" /></a>
							<div class="banner-info text-center">
								<h2 class="title30 black">shoes women</h2>
								<a href="#" class="title14 btn-caret black">shop now <i class="fa fa-caret-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="banner-adv item-adv3 zoom-image">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ad3.jpg" alt="" /></a>
							<div class="banner-info text-center">
								<h2 class="title30 white">shoes kids</h2>
								<a href="#" class="title14 btn-caret white">shop now <i class="fa fa-caret-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End List Ads3 -->
			<div class="product-box3">
				<h2 class="title18">Best sellers</h2>
				<div class="title-box3">
					<ul class="list-inline-block text-center">
						<li class="active"><a href="#tab1" class="title14 black" data-toggle="tab">Men’s </a></li>
						<li><a href="#tab2" class="title14 black" data-toggle="tab">Women’s</a></li>
						<li><a href="#tab1" class="title14 black" data-toggle="tab">Kids</a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div id="tab1" class="tab-pane fade in active">
						<div class="product-slider">
							<div class="wrap-item group-navi" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3],[1200,4]]">
								<div class="item">
									<div class="item-product text-center">
										<span class="product-label sale-label">50% off</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_3.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<span class="product-label new-label">new</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_1.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_6.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_10.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_18.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_17.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_16.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_15.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_14.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_13.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_12.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_11.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
							</div>
						</div>
					</div>
					<!-- End Tab -->
					<div id="tab2" class="tab-pane fade in">
						<div class="product-slider">
							<div class="wrap-item group-navi" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3],[1200,4]]">
								<div class="item">
									<div class="item-product text-center">
										<span class="product-label sale-label">50% off</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_1.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<span class="product-label new-label">new</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_2.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_3.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_4.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_5.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_6.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_7.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_8.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_9.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_10.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_11.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_12.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
							</div>
						</div>
					</div>
					<!-- End Tab -->
				</div>
			</div>
			<!-- End Product Box -->
			<div class="list-adv3">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="banner-adv fade-in-out">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ads1.jpg" alt="" /></a>
						</div>
					</div>
					<div class="col-md-6 hidden-sm hidden-xs">
						<div class="banner-adv fade-out-in">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ads2.jpg" alt="" /></a>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="banner-adv fade-in-out">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ads3.jpg" alt="" /></a>
						</div>
					</div>
					<div class="hidden-lg hidden-md col-sm-12">
						<div class="banner-adv fade-out-in">
							<a href="#" class="adv-thumb-link"><img src="images/home/home3/ads2.jpg" alt="" /></a>
						</div>
					</div>
				</div>
			</div>
			<!-- End List ADv -->
			<div class="product-box3">
				<h2 class="title18">featured</h2>
				<div class="title-box3">
					<ul class="list-inline-block text-center">
						<li class="active"><a href="#tab4" class="title14 black" data-toggle="tab">Men’s </a></li>
						<li><a href="#tab3" class="title14 black" data-toggle="tab">Women’s </a></li>
						<li><a href="#tab4" class="title14 black" data-toggle="tab">Kids</a></li>
					</ul>
				</div>
				<div class="tab-content">
					<div id="tab3" class="tab-pane fade in">
						<div class="product-slider">
							<div class="wrap-item group-navi" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3],[1200,4]]">
								<div class="item">
									<div class="item-product text-center">
										<span class="product-label sale-label">50% off</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_3.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<span class="product-label new-label">new</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_1.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_6.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_10.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_18.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_17.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_16.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_15.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_14.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_13.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_12.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_11.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
							</div>
						</div>
					</div>
					<!-- End Tab -->
					<div id="tab4" class="tab-pane fade in active">
						<div class="product-slider">
							<div class="wrap-item group-navi" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3],[1200,4]]">
								<div class="item">
									<div class="item-product text-center">
										<span class="product-label sale-label">50% off</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_1.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<span class="product-label new-label">new</span>
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_2.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_3.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_4.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_5.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_6.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_7.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_8.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_9.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_10.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
								<div class="item">
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_11.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
									<div class="item-product text-center">
										<div class="product-thumb">
											<a href="detail.html" class="product-thumb-link zoom-thumb">
												<img src="images/photos/sport_12.jpg" alt="">
											</a>
											<div class="product-extra-link">
												<a href="#" class="wishlist-link"></a>
												<a href="#" class="addcart-link">Add to cart</a>
												<a href="#" class="compare-link"></a>
											</div>
											<a href="quick-view.html" class="quickview-link title14 fancybox fancybox.iframe">Quick view</a>
										</div>
										<div class="product-info">
											<h3 class="product-title title14"><a href="detail.html">Sport product Name</a></h3>
											<div class="product-price">
												<del><span class="title14 silver">$798.00</span></del>
												<ins><span class="title14 color">$399.00</span></ins>
											</div>
											<div class="product-rate">
												<div class="product-rating" style="width:100%"></div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Item -->
							</div>
						</div>
					</div>
					<!-- End Tab -->
				</div>
			</div>
			<!-- End Product Box -->
			<div class="testomo-blog">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="from-blog3">
							<h2 class="title18">LATEST FROM THE BLOG</h2>
							<div class="blog-slider3">
								<div class="wrap-item group-navi" data-itemscustom="[[0,1]]" data-pagination="false" data-navigation="true">
									<div class="item">
										<div class="post-item3 table">
											<div class="post-thumb">
												<a href="#" class="post-thumb-link"><img src="images/photos/blog/b1.jpg" alt=""></a>
											</div>
											<div class="post-info">
												<h3 class="title14"><a href="#">Cupidatat non proident amet conse ctetur adipisicing</a></h3>
												<ul class="list-inline-block silver post-comment-date">
													<li><i class="fa fa-comment-o" aria-hidden="true"></i><span>8</span></li>
													<li><i class="fa fa-calendar-o" aria-hidden="true"></i><span>24/11/2017</span></li>
												</ul>
											</div>
										</div>
										<div class="post-item3 table">
											<div class="post-thumb">
												<a href="#" class="post-thumb-link"><img src="images/photos/blog/b2.jpg" alt=""></a>
											</div>
											<div class="post-info">
												<h3 class="title14"><a href="#">amet conse ctetur adipisicing Cupidatat non </a></h3>
												<ul class="list-inline-block silver post-comment-date">
													<li><i class="fa fa-comment-o" aria-hidden="true"></i><span>8</span></li>
													<li><i class="fa fa-calendar-o" aria-hidden="true"></i><span>24/11/2017</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="post-item3 table">
											<div class="post-thumb">
												<a href="#" class="post-thumb-link"><img src="images/photos/blog/b3.jpg" alt=""></a>
											</div>
											<div class="post-info">
												<h3 class="title14"><a href="#">Cupidatat non proident amet conse ctetur adipisicing</a></h3>
												<ul class="list-inline-block silver post-comment-date">
													<li><i class="fa fa-comment-o" aria-hidden="true"></i><span>8</span></li>
													<li><i class="fa fa-calendar-o" aria-hidden="true"></i><span>24/11/2017</span></li>
												</ul>
											</div>
										</div>
										<div class="post-item3 table">
											<div class="post-thumb">
												<a href="#" class="post-thumb-link"><img src="images/photos/blog/b4.jpg" alt=""></a>
											</div>
											<div class="post-info">
												<h3 class="title14"><a href="#">amet conse ctetur adipisicing Cupidatat non </a></h3>
												<ul class="list-inline-block silver post-comment-date">
													<li><i class="fa fa-comment-o" aria-hidden="true"></i><span>8</span></li>
													<li><i class="fa fa-calendar-o" aria-hidden="true"></i><span>24/11/2017</span></li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="from-blog3">
							<h2 class="title18">Testimonials</h2>
							<div class="blog-slider3">
								<div class="wrap-item group-navi" data-itemscustom="[[0,1]]" data-pagination="false" data-navigation="true">
									<div class="item">
										<div class="testimo-item">
											<div class="tes-thumb table">
												<div class="av-thumb banner-adv zoom-image">
													<a href="#" class="adv-thumb-link"><img src="images/home/home3/av1.jpg" alt=""></a>
												</div>
												<div class="av-info">
													<h3 class="title14"><a href="#">Janet Cummings</a></h3>
													<span class="color">Beamsoft</span>
												</div>
											</div>
											<p class="silver">Proin urna enim, semper at egestas sed, eleen tum in justo. Mauris sed mauris bibendum est imperdiet porttitor tincidunt at lorem. </p>
										</div>
										<div class="testimo-item">
											<div class="tes-thumb table">
												<div class="av-thumb banner-adv zoom-image">
													<a href="#" class="adv-thumb-link"><img src="images/home/home3/av2.jpg" alt=""></a>
												</div>
												<div class="av-info">
													<h3 class="title14"><a href="#">David Jonson</a></h3>
													<span class="color">CEO-Sport</span>
												</div>
											</div>
											<p class="silver">Semper at egestas sed, elementum in justo. Mauris sed mauris bibendum est imperdiet porttitor tincidunt at lorem. </p>
										</div>
									</div>
									<div class="item">
										<div class="testimo-item">
											<div class="tes-thumb table">
												<div class="av-thumb banner-adv zoom-image">
													<a href="#" class="adv-thumb-link"><img src="images/home/home3/av2.jpg" alt=""></a>
												</div>
												<div class="av-info">
													<h3 class="title14"><a href="#">David Jonson</a></h3>
													<span class="color">CEO-Sport</span>
												</div>
											</div>
											<p class="silver">Semper at egestas sed, elementum in justo. Mauris sed mauris bibendum est imperdiet porttitor tincidunt at lorem. </p>
										</div>
										<div class="testimo-item">
											<div class="tes-thumb table">
												<div class="av-thumb banner-adv zoom-image">
													<a href="#" class="adv-thumb-link"><img src="images/home/home3/av1.jpg" alt=""></a>
												</div>
												<div class="av-info">
													<h3 class="title14"><a href="#">Janet Cummings</a></h3>
													<span class="color">Beamsoft</span>
												</div>
											</div>
											<p class="silver">Proin urna enim, semper at egestas sed, eleen tum in justo. Mauris sed mauris bibendum est imperdiet porttitor tincidunt at lorem. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
			<!-- End Testimo Blog -->
			<div class="hot-cat3">
				<h2 class="title18">hot categories</h2>
				<div class="hotcat-slider3">
					<div class="wrap-item group-navi" data-itemscustom="[[0,1],[480,2],[768,3],[990,4]]" data-pagination="false" data-navigation="true">
						<div class="cat-item3 border text-center">
							<div class="banner-adv zoom-out">
								<a href="#" class="adv-thumb-link">
									<img src="images/home/home3/cat1.jpg" alt="" />
									<img src="images/home/home3/cat1.jpg" alt="" />
								</a>
							</div>
							<div class="cat-title">
								<a href="#"><span class="title14">accessories</span><span class="silver">(20)</span></a>
							</div>
						</div>
						<div class="cat-item3 border text-center">
							<div class="banner-adv zoom-out">
								<a href="#" class="adv-thumb-link">
									<img src="images/home/home3/cat2.jpg" alt="" />
									<img src="images/home/home3/cat2.jpg" alt="" />
								</a>
							</div>
							<div class="cat-title">
								<a href="#"><span class="title14">shoes</span><span class="silver">(15)</span></a>
							</div>
						</div>
						<div class="cat-item3 border text-center">
							<div class="banner-adv zoom-out">
								<a href="#" class="adv-thumb-link">
									<img src="images/home/home3/cat3.jpg" alt="" />
									<img src="images/home/home3/cat3.jpg" alt="" />
								</a>
							</div>
							<div class="cat-title">
								<a href="#"><span class="title14">Clothing</span><span class="silver">(60)</span></a>
							</div>
						</div>
						<div class="cat-item3 border text-center">
							<div class="banner-adv zoom-out">
								<a href="#" class="adv-thumb-link">
									<img src="images/home/home3/cat4.jpg" alt="" />
									<img src="images/home/home3/cat4.jpg" alt="" />
								</a>
							</div>
							<div class="cat-title">
								<a href="#"><span class="title14">tennis</span><span class="silver">(32)</span></a>
							</div>
						</div>
						<div class="cat-item3 border text-center">
							<div class="banner-adv zoom-out">
								<a href="#" class="adv-thumb-link">
									<img src="images/home/home3/cat5.jpg" alt="" />
									<img src="images/home/home3/cat5.jpg" alt="" />
								</a>
							</div>
							<div class="cat-title">
								<a href="#"><span class="title14">Dress</span><span class="silver">(10)</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Hot Cat -->
		</div>
		<div class="signup-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="email-signup left-icon">
							<img class="mail-icon" src="images/icons/icon-email.png" alt="" />
							<h2 class="title14">EMAIL SIGN UP</h2>
							<form class="email-form">
								<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Enter your email" type="text">
								<input class="shop-button" value="" type="submit">
							</form>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="email-signup right-icon">
							<img class="mail-icon" src="images/icons/icon-home.png" alt="" />
							<h2 class="title14">FIND A PUMA STORE</h2>
							<form class="email-form">
								<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Google map" type="text">
								<input class="shop-button" value="" type="submit">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Signup -->
	</section>
	<!-- End Content -->

@endsection
