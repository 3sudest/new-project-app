@extends('layouts/frontend')

@section('content')
<div id="content">
        <div class="content-page woocommerce">
            <div class="container content-about ">
                <h2 class="title30 dosis-font font-bold text-uppercase text-center dark">Member</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="register-content-box">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-ms-12">
                                    <div class="check-billing">
                                        <div class="form-my-account">
                                            <form method="POST" action="{{ route('login') }}" class="block-login">
                                            @csrf
                                                <h2 class="title24 title-form-account">Login</h2>
                                                <p>
                                                    <label>Username or email address <span class="required">*</span></label>
                                                    <input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required autofocus />
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>

                                                <p>
                                                    <label>Password <span class="required">*</span></label>
                                                    <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required/>
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>
                                                <p>
                                                    <input type="submit" class="register-button" name="login" value="Login">
                                                </p>
                                                <div class="table create-account">
                                                    <div class="text-left">
                                                        <p>
                                                            <input type="checkbox"  id="remember" /> <label for="remember">Remember me</label>
                                                        </p>
                                                    </div>
                                                    <div class="text-right">
                                                        <a href="#" class="color">Lost your password?</a>
                                                    </div>
                                                </div>
                                                <h2 class="title18 social-login-title">Or login with</h2>
                                                <div class="social-login-block table text-center">
                                                    <div class="social-login-btn">
                                                        <a href="#" class="login-fb-link">Facebook</a>
                                                    </div>
                                                    <div class="social-login-btn">
                                                        <a href="#" class="login-goo-link">Google</a>
                                                    </div>
                                                </div>
                                            </form>
                                            <form method="POST" action="{{ route('register') }}" class="block-register">
                                             @csrf
                                                <h2 class="title24 title-form-account">REGISTER</h2>
                                                <p>
                                                    <label>Username <span class="required">*</span></label>
                                                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus />                    
                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>
                                                <p>
                                                    <label>Email address <span class="required">*</span></label>
                                                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required />
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>
                                                <p>
                                                    <label>Password <span class="required">*</span></label>
                                                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required />           
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </p>
                                                <p>
                                                    <label>Password Confirmation<span class="required">*</span></label>
                                                    <input type="password" name="password_confirmation" />
                                                </p>
                                                <p>
                                                    <input type="submit" class="register-button" name="register" value="Register">
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-ms-12">
                                    <div class="check-address">
                                        <div class="form-my-account check-register text-center">
                                            <h2 class="title24 title-form-account">Register</h2>
                                            <p class="desc">Registering for this site allows you to access your order status and history. Just fill in the fields below, and we’ll get a new account set up for you in no time. We will only ask you for information necessary to make the purchase process faster and easier.</p>
                                            <a href="#" class="shop-button bg-color login-to-register" data-login="Login" data-register="Register">Register</a>
                                            <p class="desc title12 silver"><i>Click to switch Register/Login</i></p>
                                        </div>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Content Page -->
    </div>
    <!-- End Content -->
@endsection
